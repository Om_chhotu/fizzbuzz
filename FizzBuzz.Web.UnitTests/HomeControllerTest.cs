﻿using FizzBuzz.Business;
using FizzBuzz.Web.Controllers;
using FizzBuzz.Web.Models;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FizzBuzz.Web.UnitTests
{
    public class HomeControllerTest
    {
        private HomeController controller;
        private Mock<IProcessor> processor;
        public InputModel input;
        List<string> resultList = new List<string>(new string[] { "1", "2", "fizz", "4", "buzz", "fizz", "7", "8", "fizz", "buzz", "11", "fizz", "13", "14", "fizzbuzz" });

        [SetUp]
        public void SetUp()
        {
            processor = new Mock<IProcessor>();
            processor.Setup(x => x.GetMessage(It.IsAny<int>())).Returns(resultList);
            controller = new HomeController(processor.Object);
        }


        [Test]
        public void RedirectToDisplayMethod_WhenInputIsValid()
        {
            //Arrange
            input = new InputModel();
            input.UserInput = 15;

            //Act
            var result = controller.OutPut(input) as RedirectToRouteResult;

            //Assert
            Assert.IsInstanceOf<RedirectToRouteResult>(result);
            Assert.AreEqual(result.RouteValues["action"], "Display");
        }

        [Test]
        public void RedirectToInputMethod_WhenInputIsNotValid()
        {
            //Arrange
            input = new InputModel();
            input.UserInput = -1;

            //Act
            controller.ModelState.AddModelError("Range", "number should be between 1 to 1000");
            var result = controller.OutPut(input) as ViewResult;

            //Assert
            Assert.IsTrue(result.ViewName == "Input");
        }

        [Test]
        public void DisplayMethodReturnExpectedView()
        {
            //Act
            var display = controller.Display(15, 1) as ViewResult;

            //Assert
            Assert.IsNotNull(display);
            Assert.IsNotNull(display.Model);
            Assert.IsTrue(display.ViewName == "Display");
        }
    }
}
