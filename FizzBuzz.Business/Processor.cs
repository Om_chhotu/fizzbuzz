﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz.Business
{
    public class Processor:IProcessor
    {
        private IList<IDivisibilityRule> divisibilityRule;
        List<string> message = new List<string>();

        public Processor(IList<IDivisibilityRule> divisibilityRule)
        {
            this.divisibilityRule = divisibilityRule;
        }

        public IList<string> GetMessage(int number)
        {
            for (int index = 1; index <= number; index++)
            {
                var result = this.divisibilityRule.FirstOrDefault(x => x.IsMatch(index));
                message.Add(result.Execute(index));
            }
            return message;
        }
    }
}
