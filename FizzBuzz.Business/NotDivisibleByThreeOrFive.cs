﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz.Business
{
    public class NotDivisibleByThreeOrFive:IDivisibilityRule
    {
        public bool IsMatch(int number)
        {
            return number % 5 != 0 && number % 3 != 0 ;
        }

        public string Execute(int number)
        {
            return IsMatch(number) ? number.ToString() : null;
        }
    }
}
