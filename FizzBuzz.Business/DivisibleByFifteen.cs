﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz.Business
{
    public class DivisibleByFifteen:IDivisibilityRule
    {
        private ISpecificDayCheck day;

        public DivisibleByFifteen(ISpecificDayCheck day)
        {
            this.day = day;
        }

        public bool IsMatch(int number)
        {
            return number % 15 == 0 ;
        }

        public string Execute(int number)
        {
            if (IsMatch(number))
            {
                return day.IsWednesday(DateTime.Now.DayOfWeek) ? "wizzwuzz" : "fizzbuzz";

            }
            else
            {
                return null;
            }
        }
    }
}
