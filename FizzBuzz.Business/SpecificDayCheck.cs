﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz.Business
{
    public class SpecificDayCheck : ISpecificDayCheck
    {
        public bool IsWednesday(DayOfWeek day)
        {
            return day == DayOfWeek.Wednesday;
        }
    }
}
