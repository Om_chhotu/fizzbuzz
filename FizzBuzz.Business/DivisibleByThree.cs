﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz.Business
{
    public class DivisibleByThree:IDivisibilityRule
    {
        private ISpecificDayCheck day;

        public DivisibleByThree(ISpecificDayCheck day)
        {
            this.day = day;
        }

        public bool IsMatch(int number)
        {
            return number % 3 == 0 && number % 5 != 0;
        }

        public string Execute(int number)
        {
            if (IsMatch(number))
            {
                return day.IsWednesday(DateTime.Now.DayOfWeek) ? "wizz" : "fizz";

            }
            else
            {
                return null;
            }
        }
    }
}
