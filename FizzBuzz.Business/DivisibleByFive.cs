﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz.Business
{
    public class DivisibleByFive:IDivisibilityRule
    {
        private ISpecificDayCheck day;

        public DivisibleByFive(ISpecificDayCheck day)
        {
            this.day = day;
        }

        public bool IsMatch(int number)
        {
            return number % 5 == 0 && number % 3 != 0;
        }

        public string Execute(int number)
        {
            if (IsMatch(number))
            {
                return day.IsWednesday(DateTime.Now.DayOfWeek) ? "wuzz" : "buzz";

            }
            else
            {
                return null;
            }
        }
    }
}
