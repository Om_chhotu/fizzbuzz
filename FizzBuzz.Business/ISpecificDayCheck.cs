﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz.Business
{
    public interface ISpecificDayCheck
    {
        bool IsWednesday(DayOfWeek day);
    }
}
