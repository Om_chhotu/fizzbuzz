﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
namespace FizzBuzz.Business.UnitTests
{
    [TestFixture]
    public class SpecificDayCheckTest
    {
        private ISpecificDayCheck specificDayCheck;

        [SetUp]
        public void SetUp()
        {
            specificDayCheck = new SpecificDayCheck();
        }

        [TestCase(DayOfWeek.Wednesday, true)]
        public void ShouldReturnTrueWhenDayIsWednbesday(DayOfWeek testDay, bool expectedResult)
        {
            //Act
            var result = specificDayCheck.IsWednesday(testDay);

            //Assert
            Assert.AreEqual(result, expectedResult);
        }

        [TestCase(DayOfWeek.Monday, false)]
        [TestCase(DayOfWeek.Tuesday, false)]
        [TestCase(DayOfWeek.Friday, false)]
        [TestCase(DayOfWeek.Thursday, false)]
        [TestCase(DayOfWeek.Saturday, false)]
        [TestCase(DayOfWeek.Sunday, false)]
        public void ShouldReturnFalseWhenDayIsNotWednbesday(DayOfWeek testDay, bool expectedResult)
        {
            //Act
            var result = specificDayCheck.IsWednesday(testDay);

            //Assert
            Assert.AreEqual(result, expectedResult);
        }
    }
}
