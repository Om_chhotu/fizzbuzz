﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
namespace FizzBuzz.Business.UnitTests
{
    [TestFixture]
    public class ProcessorTest
    {
        IProcessor processor;
        List<string> resultList = new List<string>(new string[] { "1", "2", "fizz", "4", "buzz", "fizz", "7", "8", "fizz", "buzz", "11", "fizz", "13", "14", "fizzbuzz" });
        List<int> listThree = new List<int>(new int[] { 3, 6, 9, 12 });
        List<int> listFive = new List<int>(new int[] { 5, 10 });

        [SetUp]
        public void SetUp()
        {
            var mockIsDivisibleBy = new Mock<IDivisibilityRule>();
            mockIsDivisibleBy.Setup(x => x.IsMatch(It.IsAny<int>())).Returns(true);
            mockIsDivisibleBy.Setup(x => x.Execute(It.Is<int>(y => listThree.Contains(y)))).Returns("fizz");
            mockIsDivisibleBy.Setup(x => x.Execute(It.Is<int>(y => listFive.Contains(y)))).Returns("buzz");
            mockIsDivisibleBy.Setup(x => x.Execute(It.Is<int>(y => y == 1))).Returns("1");
            mockIsDivisibleBy.Setup(x => x.Execute(It.Is<int>(y => y == 2))).Returns("2");
            mockIsDivisibleBy.Setup(x => x.Execute(It.Is<int>(y => y == 4))).Returns("4");
            mockIsDivisibleBy.Setup(x => x.Execute(It.Is<int>(y => y == 7))).Returns("7");
            mockIsDivisibleBy.Setup(x => x.Execute(It.Is<int>(y => y == 8))).Returns("8");
            mockIsDivisibleBy.Setup(x => x.Execute(It.Is<int>(y => y == 11))).Returns("11");
            mockIsDivisibleBy.Setup(x => x.Execute(It.Is<int>(y => y == 13))).Returns("13");
            mockIsDivisibleBy.Setup(x => x.Execute(It.Is<int>(y => y == 14))).Returns("14");
            mockIsDivisibleBy.Setup(x => x.Execute(It.Is<int>(y => y == 15))).Returns("fizzbuzz");

            var list = new List<IDivisibilityRule> { mockIsDivisibleBy.Object };
            processor = new Processor(list);
        }

        [Test]
        public void GenerateResultListTest()
        {
            //Act
            var result = processor.GetMessage(15);

            //Assert
            Assert.AreEqual(result, resultList);
        }
    }
}
