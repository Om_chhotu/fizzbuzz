﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
namespace FizzBuzz.Business.UnitTests
{
    public class NotDivisibleByThreeOrFiveTest
    {
        IDivisibilityRule notDivisibleByThreeOrFive;

        [SetUp]
        public void Setup()
        {
            notDivisibleByThreeOrFive = new NotDivisibleByThreeOrFive();
        }

        [TestCase(8, true)]
        [TestCase(4, true)]
        [TestCase(121, true)]
        [TestCase(5, false)]
        [TestCase(3, false)]
        [TestCase(9, false)]
        [TestCase(15, false)]
        public void IsMatchReturnExpectedResult(int input, bool expectedResult)
        {
            //Act
            var result = notDivisibleByThreeOrFive.IsMatch(input);

            //Assert
            Assert.AreEqual(result, expectedResult);
        }

        [TestCase(71, "71")]
        [TestCase(8, "8")]
        [TestCase(25, null)]
        [TestCase(15, null)]
        [TestCase(10, null)]
        [TestCase(44, "44")]
        [TestCase(3, null)]
        public void ExecuteReturnExpectedResult(int input, string expectedResult)
        {
            //Act
            var result = notDivisibleByThreeOrFive.Execute(input);

            //Assert
            Assert.AreEqual(result, expectedResult);
        }
    }
}
