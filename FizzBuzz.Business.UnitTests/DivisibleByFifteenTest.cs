﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;

namespace FizzBuzz.Business.UnitTests
{
    [TestFixture]
    public class DivisibleByFifteenTest
    {
        IDivisibilityRule divisibleByFifteen;
        Mock<ISpecificDayCheck> mockDay;

        [SetUp]
        public void Setup()
        {
            mockDay = new Mock<ISpecificDayCheck>();
            divisibleByFifteen = new DivisibleByFifteen(mockDay.Object);
        }

        [TestCase(15, true)]
        [TestCase(60, true)]
        [TestCase(90, true)]
        [TestCase(5, false)]
        [TestCase(3, false)]
        [TestCase(9, false)]
        [TestCase(4, false)]
        public void IsMatchReturnExpectedResult(int input, bool expectedResult)
        {
            //Act
            var result = divisibleByFifteen.IsMatch(input);

            //Assert
            Assert.AreEqual(result, expectedResult);
        }

        [TestCase(75, "wizzwuzz")]
        [TestCase(90, "wizzwuzz")]
        [TestCase(3, null)]
        [TestCase(5, null)]
        [TestCase(2, null)]
        [TestCase(30, "wizzwuzz")]
        [TestCase(8, null)]
        public void ExecuteReturnExpectedResultOnWednesday(int input, string expectedResult)
        {
            //Arrange
            mockDay.Setup(x => x.IsWednesday(DateTime.Now.DayOfWeek)).Returns(true);

            //Act
            var result = divisibleByFifteen.Execute(input);

            //Assert
            Assert.AreEqual(result, expectedResult);
        }

        [TestCase(75, "fizzbuzz")]
        [TestCase(90, "fizzbuzz")]
        [TestCase(3, null)]
        [TestCase(5, null)]
        [TestCase(2, null)]
        [TestCase(30, "fizzbuzz")]
        [TestCase(8, null)]
        public void ExecuteReturnExpectedResultOnDayOtherDayThanWednesday(int input, string expectedResult)
        {
            //Arrange
            mockDay.Setup(x => x.IsWednesday(DateTime.Now.DayOfWeek)).Returns(false);

            //Act
            var result = divisibleByFifteen.Execute(input);

            //Assert
            Assert.AreEqual(result, expectedResult);
        }
    }
}
