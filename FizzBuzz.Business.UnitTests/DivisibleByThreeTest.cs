﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;

namespace FizzBuzz.Business.UnitTests
{
    [TestFixture]
    public class DivisibleByThreeTest
    {
        IDivisibilityRule divisibleByThree;
        Mock<ISpecificDayCheck> mockDay;

        [SetUp]
        public void Setup()
        {
            mockDay = new Mock<ISpecificDayCheck>();
            divisibleByThree = new DivisibleByThree(mockDay.Object);
        }

        [TestCase(3, true)]
        [TestCase(9, true)]
        [TestCase(33, true)]
        [TestCase(5, false)]
        [TestCase(15, false)]
        [TestCase(2, false)]
        [TestCase(8, false)]
        public void IsMatchReturnExpectedResult(int input, bool expectedResult)
        {
            //Act
            var result = divisibleByThree.IsMatch(input);

            //Assert
            Assert.AreEqual(result, expectedResult);
        }

        [TestCase(3, "wizz")]
        [TestCase(9, "wizz")]
        [TestCase(5, null)]
        [TestCase(15, null)]
        [TestCase(2, null)]
        [TestCase(33, "wizz")]
        [TestCase(8, null)]
        public void ExecuteReturnExpectedResultOnWednesday(int input, string expectedResult)
        {
            //Arrange
            mockDay.Setup(x => x.IsWednesday(DateTime.Now.DayOfWeek)).Returns(true);

            //Act
            var result = divisibleByThree.Execute(input);

            //Assert
            Assert.AreEqual(result, expectedResult);
        }

        [TestCase(3, "fizz")]
        [TestCase(9, "fizz")]
        [TestCase(5, null)]
        [TestCase(15, null)]
        [TestCase(2, null)]
        [TestCase(33, "fizz")]
        [TestCase(8, null)]
        public void ExecuteReturnExpectedResultOnDayOtherDayThanWednesday(int input, string expectedResult)
        {
            //Arrange
            mockDay.Setup(x => x.IsWednesday(DateTime.Now.DayOfWeek)).Returns(false);

            //Act
            var result = divisibleByThree.Execute(input);

            //Assert
            Assert.AreEqual(result, expectedResult);
        }
    }
}
