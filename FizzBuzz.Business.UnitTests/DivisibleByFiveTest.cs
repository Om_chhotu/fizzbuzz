﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Moq;

namespace FizzBuzz.Business.UnitTests
{
    [TestFixture]
    public class DivisibleByFiveTest
    {
        IDivisibilityRule divisibleByFive;
        Mock<ISpecificDayCheck> mockDay;

        [SetUp]
        public void Setup()
        {
            mockDay = new Mock<ISpecificDayCheck>();
            divisibleByFive = new DivisibleByFive(mockDay.Object);
        }

        [TestCase(25, true)]
        [TestCase(50, true)]
        [TestCase(5, true)]
        [TestCase(15, false)]
        [TestCase(30, false)]
        [TestCase(3, false)]
        [TestCase(8, false)]
        public void IsMatchReturnExpectedResult(int input, bool expectedResult)
        {
            //Act
            var result = divisibleByFive.IsMatch(input);

            //Assert
            Assert.AreEqual(result, expectedResult);
        }

        [TestCase(5, "wuzz")]
        [TestCase(10, "wuzz")]
        [TestCase(3, null)]
        [TestCase(15, null)]
        [TestCase(2, null)]
        [TestCase(50, "wuzz")]
        [TestCase(8, null)]
        public void ExecuteReturnExpectedResultOnWednesday(int input, string expectedResult)
        {
            //Arrange
            mockDay.Setup(x => x.IsWednesday(DateTime.Now.DayOfWeek)).Returns(true);

            //Act
            var result = divisibleByFive.Execute(input);

            //Assert
            Assert.AreEqual(result, expectedResult);
        }

        [TestCase(5, "buzz")]
        [TestCase(10, "buzz")]
        [TestCase(3, null)]
        [TestCase(15, null)]
        [TestCase(2, null)]
        [TestCase(50, "buzz")]
        [TestCase(8, null)]
        public void ExecuteReturnExpectedResultOnDayOtherDayThanWednesday(int input, string expectedResult)
        {
            //Arrange
            mockDay.Setup(x => x.IsWednesday(DateTime.Now.DayOfWeek)).Returns(false);

            //Act
            var result = divisibleByFive.Execute(input);

            //Assert
            Assert.AreEqual(result, expectedResult);
        }
    }
}
