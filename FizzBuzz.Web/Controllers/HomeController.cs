﻿using FizzBuzz.Business;
using FizzBuzz.Web.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FizzBuzz.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IProcessor processor;

        public HomeController(IProcessor processor)
        {
            this.processor = processor;
        }

        public ActionResult Input()
        {
            return View(new InputModel());
        }

        public ActionResult OutPut(InputModel input)
        {
            if (!ModelState.IsValid)
            {
                return View("Input", input);
            }

            return RedirectToAction("Display", new { number = input.UserInput, page = 1 });
        }

        public ActionResult Display(int number, int? page)
        {
            var message = processor.GetMessage(number);
            var model = new DisplayViewModel() { Number = number, Message = message.ToPagedList(page ?? 1, 20) };
            return View("Display", model);
        }
    }
}