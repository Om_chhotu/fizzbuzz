﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FizzBuzz.Web.Models
{
    public class InputModel
    {
        [Required(ErrorMessage = "please enter a valid number")]
        [Range(1, 1000, ErrorMessage = "the number must be between 1 and 1000")]
        public int UserInput { get; set; }
    }
}