﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FizzBuzz.Web.Models
{
    public class DisplayViewModel
    {
        public IPagedList<string> Message { get; set; }
        public int Number { get; set; }
    }
}